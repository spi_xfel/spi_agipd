#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create new SPI processing
Author: Sergey Bobkov
"""

from __future__ import print_function
import os
import sys
import argparse
from shutil import copyfile, copytree

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/pysrc')
from libs.config import SpiConfig, CONFIG_FILE

DEFAULT_CONFIG = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data/defaults.ini')


def create_processing_dir(prefix, tag):
    i = 1
    while True:
        dirname = os.path.join(prefix, '{}_{:02d}'.format(tag, i))
        if os.path.exists(dirname):
            i += 1
        else:
            os.makedirs(dirname)
            return dirname


def main():
    parser = argparse.ArgumentParser(description='Create new SPI processing')
    parser.add_argument('-r', '--raw', dest='raw', required=True, help='RAW data folder')
    parser.add_argument('-t', '--tag', dest='tag', help='Folder tag')
    parser.add_argument('-p', '--prefix', dest='prefix', default='./', help='Base directory')
    parser_args = parser.parse_args()

    raw_dir = parser_args.raw
    tag = parser_args.tag
    prefix = parser_args.prefix

    if not os.path.isdir(raw_dir):
        parser.error("{} is not a valid RAW directory".format(raw_dir))

    if tag is None:
        tag = os.path.basename(raw_dir)

    if not os.path.isdir(prefix):
        parser.error("{} is not a valid directory".format(prefix))

    dirname = create_processing_dir(prefix, tag)

    print('New SPI processing in {}'.format(dirname))

    config = SpiConfig(DEFAULT_CONFIG)
    config.set_raw_directory(os.path.realpath(raw_dir))
    config.save_config(os.path.join(dirname, CONFIG_FILE))

    src_dir = os.path.dirname(os.path.abspath(__file__))

    for fname in ['pysrc']:
        os.symlink(os.path.join(src_dir, fname), os.path.join(dirname, fname))

    # for fname in ['shsrc']:
    #     copytree(os.path.join(src_dir, fname), os.path.join(dirname, fname))

    for fname in ['agipd.geom']:
        copyfile(os.path.join(src_dir, 'data', fname), os.path.join(dirname, fname))


if __name__ == '__main__':
    main()
