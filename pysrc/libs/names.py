#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Lib for name generation
Author: Sergey Bobkov
"""

def generate_calib_filename(calib_dir, run, panel):
    calib_run_folder = '{}/r{:04d}'.format(calib_dir, run)
    return '{}/Calib-AGIPD{:02d}.h5'.format(calib_run_folder, panel)


def generate_hits_filename(hits_dir, run):
    return '{}/hits_r{:04d}.cxi'.format(hits_dir, run)


def generate_hits_series_filename(hits_dir, run, series):
    return '{}/series_data/hits_r{:04d}_s{:05d}.cxi'.format(hits_dir, run, series)
