#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import datetime
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.colors as colors
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

mpl.rcParams['image.cmap'] = 'viridis'


class ReportWriter(object):
    """Produce PDF reports with tables and matplotlib figures"""
    def __init__(self, file_name):
        self._create_file(file_name)

    def _create_file(self, file_name):
        self.pdf_pages = PdfPages(file_name)
        pdf_dict = self.pdf_pages.infodict()
        pdf_dict['CreationDate'] = datetime.datetime.today()
        pdf_dict['ModDate'] = datetime.datetime.today()

    def save_table(self, data_frame):
        """Add a page to PDF with table

        Keyword arguments:
        data_frame -- pandas dataframe to be saved
        """
        # Compute optimal columns width
        content_df = data_frame.copy()
        content_df.loc[len(content_df)] = data_frame.columns.values
        col_widhts = np.array([content_df.loc[:, c].apply(str).apply(len).max()\
                               for c in content_df.columns.values], dtype=float)
        col_widhts *= 1/col_widhts.sum()

        fig, ax = plt.subplots(figsize=(10, len(content_df)/4 + 1))
        ax.axis('off')
        ax.axis('tight')

        ax.table(cellText=data_frame.values, colLabels=data_frame.columns, loc='center',
                 colWidths=col_widhts)

        fig.tight_layout()

        self.pdf_pages.savefig()

    def save_figure(self, figure):
        """Add a page to PDF with matplotlib figure

        Keyword arguments:
        figure -- figure to be saved
        """
        # figure.tight_layout()
        self.pdf_pages.savefig(figure)
        plt.close(figure)

    def save_mask(self, mask_data):
        fig, ax = plt.subplots(figsize=(8,8))

        cmap = colors.ListedColormap([[0, 0.7, 0],\
                                      [0, 0, 0.7],\
                                      [0.7, 0, 0]])
        bounds = [0, 0.5, 1.5, 2]
        norm = colors.BoundaryNorm(bounds, cmap.N)

        ax.imshow(mask_data, interpolation='nearest',
                  cmap=cmap, norm=norm)

        ax.set_xticks([])
        ax.set_yticks([])

        handles = []
        if (mask_data == 0).any():
            handles.append(mpatches.Patch(color=[0, 0.7, 0],
                                          label='Good pixels'))
        if (mask_data == 1).any():
            handles.append(mpatches.Patch(color=[0, 0, 0.7],
                                          label='Non-orientationally relevant pixels'))
        if (mask_data == 2).any():
            handles.append(mpatches.Patch(color=[0.7, 0, 0],
                                          label='Bad pixels'))
        ax.legend(handles=handles)

        ax.set_title('Mask')
        fig.tight_layout()
        self.pdf_pages.savefig()

    def close(self):
        """Closes PDF document"""
        self.pdf_pages.close()


def plot_image(image, axis, logscale=True, colorbar=True, vmin=None, vmax=None):
    """Plot image to axis with optional logscale and colorbar

    Keyword arguments:
    image -- 2D array with data to show
    axis -- matplotlib axis to plot
    logscale -- use logscale
    colorbar -- add colorbar to image
    vmin -- minimal value of colormap
    vmax -- miximum value of colormap
    """
    if vmin is None and logscale:
        vmin = max(1, np.amin(image))
    elif vmin is None:
        vmin = np.amin(image)

    if vmax is None:
        vmax = np.amax(image)

    if logscale:
        norm = colors.LogNorm(vmin=vmin, vmax=vmax)
    else:
        norm = colors.Normalize(vmin=vmin, vmax=vmax)

    im_handle = axis.imshow(image, norm=norm, interpolation='nearest')
    axis.set_xticks([])
    axis.set_yticks([])

    if colorbar:
        divider = make_axes_locatable(axis)
        cb_axis = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(im_handle, cax=cb_axis)
