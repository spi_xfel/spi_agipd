#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Read raw data and apply calibration
Author: Sergey Bobkov
"""

import os
import glob
import h5py
import numpy as np

from . import log


class AGIPDReader():
    """Reader for RAW and PROC files of AGIPD detector on European XFEL"""
    def __init__(self, data_dir, raw_data):
        self.cell_ids = None
        self.raw_data = raw_data
        assert os.path.isdir(data_dir)
        self.data_dir = data_dir


    def set_cell_filter(self, cell_ids):
        """Set ids for good cells, it will be filtered on read"""
        self.cell_ids = cell_ids


    def get_runs(self):
        """Get available runs for selected data folder"""
        run_number = [int(dname[1:])
                      for dname in os.listdir(self.data_dir)
                      if dname[0] == 'r' and
                      dname[1:].isdigit() and
                      os.path.isdir(os.path.join(self.data_dir, dname))]

        return sorted(run_number)


    def get_num_series_panels(self, run, verify_run=False):
        """Get number of series and panels in run, return (0,0) if run is damaged"""
        run_dir = '{0}/r{1:04d}'.format(self.data_dir, run)
        if not os.path.isdir(run_dir):
            raise ValueError('Directory does not exist: {}'.format(run_dir))

        if self.raw_data:
            glob_line = '{0}/RAW-R{1:04d}-AGIPD*.h5'.format(run_dir, run)
        else:
            glob_line = '{0}/CORR-R{1:04d}-AGIPD*.h5'.format(run_dir, run)

        run_files = glob.glob(glob_line)

        if not run_files:
            return 0, 0

        panel_num = np.zeros(len(run_files), dtype=int)
        series_num = panel_num.copy()

        for i, run_f in enumerate(run_files):
            base_f = os.path.basename(run_f)
            panel_num[i] = int(base_f.split('-')[2][-2:])
            series_num[i] = int(base_f.split('.')[0].split('-')[3][1:])

        n_panels = np.unique(panel_num).size
        n_series = np.unique(series_num).size

        if verify_run:
            for i in range(n_panels):
                for j in range(n_series):
                    n_files = np.sum(np.logical_and(panel_num == i, series_num == j))
                    if n_files != 1:
                        log.warning('Missed file for run {} panel {} series {}'.format(run, i, j))

        return n_series, n_panels


    def get_num_frames(self, run, series, panel):
        """Get number of frames in data file"""
        data_file = self._data_filename(run, panel, series)
        data_group = self._data_group_path(panel)
        data_dset = data_group + '/data'

        if not os.path.isfile(data_file):
            return 0

        with h5py.File(data_file, 'r') as h5file:
            h5_series_data = h5file[data_dset]
            return len(h5_series_data)


    def read_ids(self, run, series, panel):
        """Returns indexes for frames in data file"""
        data_file = self._data_filename(run, panel, series)
        data_group = self._data_group_path(panel)
        dset_cell_id = data_group + '/cellId'
        dset_train_id = data_group + '/trainId'
        dset_pulse_id = data_group + '/pulseId'

        with h5py.File(data_file, 'r') as h5file:
            cell_id_data = h5file[dset_cell_id][:].flatten()
            train_id_data = h5file[dset_train_id][:].flatten()
            pulse_id_data = h5file[dset_pulse_id][:].flatten()

            total_cells = len(np.unique(cell_id_data))
            assert np.amax(cell_id_data) == total_cells - 1, 'Bad cell order'
            assert np.amin(cell_id_data) == 0, 'Bad cell order'

            train_ids = train_id_data[cell_id_data == 1]
            cell_ids = cell_id_data[train_id_data == train_ids[0]]
            pulse_ids = pulse_id_data[train_id_data == train_ids[0]]

        if self.cell_ids is not None:
            cell_filter = np.zeros(total_cells, dtype=bool)
            cell_filter[self.cell_ids] = True
            cell_ids = cell_ids[cell_filter]
            pulse_ids = pulse_ids[cell_filter]

        return train_ids, cell_ids, pulse_ids


    def get_index(self, run, panel, train_ids, cell_ids):
        """Find ids and series for frames by train and cell ids"""
        assert len(train_ids) == len(cell_ids)
        num_images = len(train_ids)

        num_series, num_panels = self.get_num_series_panels(run)
        assert panel < num_panels

        result_series = np.full(num_images, -1, dtype=int)
        result_indexes = result_series.copy()

        for series in range(num_series):
            if self.raw_data:
                data_file = self._raw_data_filename(run, panel, series)
            else:
                data_file = self._proc_data_filename(run, panel, series)

            if not os.path.isfile(data_file):
                continue

            data_group = AGIPDReader._data_group_path(panel)
            dset_cell_id = data_group + '/cellId'
            dset_train_id = data_group + '/trainId'

            with h5py.File(data_file, 'r') as h5file:
                cell_id_data = h5file[dset_cell_id][:]
                train_id_data = h5file[dset_train_id][:]

                for i in range(num_images):
                    matched_indexes = np.nonzero(
                        np.logical_and(cell_id_data == cell_ids[i],
                                       train_id_data == train_ids[i]))[0]

                    if len(matched_indexes) > 1:
                        log.warning('Found {} images with train {} and cell {}'.format(
                            len(matched_indexes), train_ids[i], cell_ids[i]))
                    elif len(matched_indexes) == 1:
                        result_series[i] = series
                        result_indexes[i] = matched_indexes[0]

        return result_series, result_indexes


    def get_image_shape(self, run, panel=0, series=0):
        """Returns shape of images in data file"""
        data_file = self._data_filename(run, panel, series)
        data_group = self._data_group_path(panel)
        data_dset = data_group + '/data'

        with h5py.File(data_file, 'r') as h5file:
            h5_series_data = h5file[data_dset]

            return h5_series_data.shape[-2:]


    def read_series(self, run, series, panel, train_ids=None):
        """Reads all frames from data file as 4d array [train, cell, height, width]"""
        data_file = self._data_filename(run, panel, series)
        data_group = self._data_group_path(panel)
        data_dset = data_group + '/data'
        dset_cell_id = data_group + '/cellId'
        dset_train_id = data_group + '/trainId'

        with h5py.File(data_file, 'r') as h5file:
            h5_series_data = h5file[data_dset]
            cell_id_data = h5file[dset_cell_id][:].flatten()
            train_id_data = h5file[dset_train_id][:].flatten()

            if train_id_data.size == 0:
                return None, None

            total_cells = len(np.unique(cell_id_data))
            assert total_cells == np.max(cell_id_data) + 1, 'Bad cell order'

            if train_ids is not None:
                series_data = h5_series_data[np.isin(train_id_data, train_ids), ...]
            else:
                series_data = h5_series_data[:]
                series_data = series_data[train_id_data > 0, ...]
                # series_data = series_data[train_id_data > 0, ...]
            series_data = np.reshape(series_data, (-1, total_cells) + h5_series_data.shape[1:])

        if self.cell_ids is not None:
            cell_filter = np.zeros(total_cells, dtype=bool)
            cell_filter[self.cell_ids] = True
            series_data = series_data[:, cell_filter]

        return series_data


    def read_mask(self, run, series, panel):
        """Reads all masks from Processed data file as 4d array [cell, height, width]"""
        if self.raw_data:
            raise ValueError('No mask available for RAW data')

        data_file = self._data_filename(run, panel, series)
        data_group = self._data_group_path(panel)
        dset_mask = data_group + '/mask'
        dset_cell_id = data_group + '/cellId'

        with h5py.File(data_file, 'r') as h5file:
            mask_data = h5file[dset_mask]
            cell_id_data = h5file[dset_cell_id][:].flatten()
            total_cells = len(np.unique(cell_id_data))
            mask_data = mask_data[:total_cells]

        if self.cell_ids is not None:
            cell_filter = np.zeros(total_cells, dtype=bool)
            cell_filter[self.cell_ids] = True
            mask_data = mask_data[cell_filter]

        return mask_data


    def read_image(self, run, series, panel, idx):
        """Reads one frame from data file"""
        data_file = self._data_filename(run, panel, series)
        data_group = self._data_group_path(panel)
        data_dset = data_group + '/data'

        with h5py.File(data_file, 'r') as h5file:
            image_data = h5file[data_dset][idx]

        return image_data

    def _data_filename(self, run, panel, series):
        if self.raw_data:
            return self._raw_data_filename(run, panel, series)
        else:
            return self._proc_data_filename(run, panel, series)

    def _raw_data_filename(self, run, panel, series):
        return '{0}/r{1:04d}/RAW-R{1:04d}-AGIPD{2:02d}-S{3:05d}.h5'.format(self.data_dir,
                                                                           run, panel, series)

    def _proc_data_filename(self, run, panel, series):
        return '{0}/r{1:04d}/CORR-R{1:04d}-AGIPD{2:02d}-S{3:05d}.h5'.format(self.data_dir,
                                                                            run, panel, series)

    @staticmethod
    def _data_group_path(panel):
        return '/INSTRUMENT/SPB_DET_AGIPD1M-1/DET/{:d}CH0:xtdf/image'.format(panel)
