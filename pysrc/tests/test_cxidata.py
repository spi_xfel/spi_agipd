#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Tests for agipd.py
Author: Sergey Bobkov
"""

import os
import sys
import numpy as np
import h5py
import random
import string

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs import cxidata

def test_create(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename, rewrite=True)

    with h5py.File(filename, 'r') as h5file:
        assert 'entry_1' in h5file

def test_read_write(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    path = 'group/to/the/data'
    data = np.random.randint(1, 100, size=(20, 20))
    cxidata.save_dataset(filename, path, data)

    read_data = cxidata.read_dataset(filename, path)
    assert (data == read_data).all()

def test_soft_link(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    path = 'group/to/the/data'
    link = 'another_group/link_data'
    data = np.random.randint(1, 100, size=(20, 20))
    cxidata.save_dataset(filename, path, data)
    cxidata.create_soft_link(filename, link, path)

    read_data = cxidata.read_dataset(filename, link)
    assert (data == read_data).all()

def test_dset_shape(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    path = 'group/to/the/data'
    shape = (np.random.randint(1, 100), np.random.randint(1, 100))
    data = np.random.randint(1, 100, size=shape)
    cxidata.save_dataset(filename, path, data)

    assert shape == cxidata.get_dataset_shape(filename, path)


def randomString(stringLength):
    """Generate a random string with the combination of lowercase and uppercase letters """

    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))


def test_group_keys(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    group = 'group/to/the/data/'
    data = np.random.randint(1, 100, size=(10, 10))

    strings = [randomString(random.randint(5, 15)) for i in range(10)]

    for s in strings:
        cxidata.save_dataset(filename, group + s, data)

    group_keys = cxidata.get_group_keys(filename, group)

    assert len(group_keys) == len(strings)

    for s in strings:
        assert s in group_keys
