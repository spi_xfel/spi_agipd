#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Delete unassembled data found hits
Author: Sergey Bobkov
"""

import sys
import os
import argparse
from lockfile import LockFile, AlreadyLocked

import hitdata

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs.config import SpiConfig, CONFIG_FILE
from libs import names, agipd, log


def delete_panels_run(spi_config, run):
    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)
    n_series, _ = det_reader.get_num_series_panels(run)

    for series in range(n_series):
        hit_file = names.generate_hits_series_filename(spi_config.get_hits_directory(), run, series)

        if not os.path.isfile(hit_file):
            log.error('No hits data for run {} series {}'.format(run, series))
            return -1

        lock = LockFile(hit_file)
        try:
            lock.acquire(timeout=0)
            hitdata.clear_instrument_data(hit_file)
            lock.release()
        except AlreadyLocked:
            continue
        except:
            lock.release()
            raise


def main():
    parser = argparse.ArgumentParser(description="Delete unassembled panels data from cxi files")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    parser.add_argument("-r", "--run", type=int, nargs='+', help="Run to process")
    args = parser.parse_args()

    config_file = args.config_file
    data_runs = args.run

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)
    log.setup_logging(spi_config.get_log_file())

    if data_runs is None:
        sample_runs = spi_config.get_sample_runs()
        data_runs = sum(sample_runs.values(), [])

    for run in data_runs:
        delete_panels_run(spi_config, run)


if __name__ == '__main__':
    main()
