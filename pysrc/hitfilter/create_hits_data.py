#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Find hits in data by number of litpixels
Author: Sergey Bobkov
"""
import os
import sys
import argparse
from lockfile import LockFile, AlreadyLocked

import hitdata
import assemble_detector
import find_hits
import save_hits_data
import combine_run_data

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs.config import SpiConfig, CONFIG_FILE
from libs import names, agipd, log
from calibration import calibration


def compute_hits_run(spi_config, run, series, num_panels, calib_run):
    hit_file = names.generate_hits_series_filename(spi_config.get_hits_directory(), run, series)
    run_status = hitdata.get_hit_status(hit_file)
    panel = spi_config.get_brightest_panel()

    if run_status == hitdata.HitStatus.EMPTY:
        log.debug('Started hit-filtering for run {} series {}'.format(run, series))
        hit_train, hit_cell = find_hits.find_hits_ids(spi_config,
                                                      run, series, panel, calib_run)
        if hit_train.size > 0:
            hitdata.save_hits_ids(hit_file, hit_train, hit_cell)
        else:
            hitdata.save_hits_ids(hit_file, None, None)

        log.info('Finished hit-filtering for run {} series {}'.format(run, series))
    if run_status in [hitdata.HitStatus.EMPTY, hitdata.HitStatus.FOUND_IDS]:
        log.debug('Started saving hits for run {} series {}'.format(run, series))
        # Reversed for saving 0 panel last, need it to check hit status
        for panel in reversed(range(num_panels)):
            save_hits_data.save_hits_panel(spi_config, run, series, panel, calib_run)
            log.debug('Data saved for panel {}'.format(panel))

        log.info('Hits saved for run {} series {}'.format(run, series))
    if run_status in [hitdata.HitStatus.EMPTY,
                      hitdata.HitStatus.FOUND_IDS,
                      hitdata.HitStatus.SAVED]:
        log.debug('Assembling data for run {} series {}'.format(run, series))
        assemble_detector.assemble_data_series(spi_config, run, series)
        hitdata.clear_instrument_data(hit_file)
        log.info('Data assembled for run {} series {}'.format(run, series))


def create_hits_run(spi_config, run, lock=True):
    hit_dir = spi_config.get_hits_directory()
    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)

    calib_run = calibration.get_best_calib_run(spi_config, run)
    if calib_run < 0:
        log.error('No calibration runs in config')
        return -1

    if not calibration.validate_calib_run(spi_config, calib_run):
        log.error('No data for calibration run {}'.format(calib_run))
        return -1

    num_series, num_panels = det_reader.get_num_series_panels(run)
    if num_series == 0:
        log.error('Bad RAW data for run: {}'.format(run))
        return -1

    panel = spi_config.get_brightest_panel()
    if panel >= num_panels:
        log.error('Wrong panel number: {} >= {}, run {}'.format(panel, num_panels, run))
        return -1

    if det_reader.get_num_frames(run, 0, panel) == 0:
        log.error('No frames for run: {}'.format(run))
        return -1

    run_hitfile = names.generate_hits_filename(hit_dir, run)
    if hitdata.get_hit_status(run_hitfile, lock) == hitdata.HitStatus.READY:
        log.debug('Run {} ready'.format(run))
        return 0

    for series in range(num_series):
        hit_file = names.generate_hits_series_filename(spi_config.get_hits_directory(), run, series)
        os.makedirs(os.path.dirname(hit_file), exist_ok=True)

        run_status = hitdata.get_hit_status(hit_file, lock)
        if run_status == hitdata.HitStatus.READY:
            continue

        if lock:
            lock_handle = LockFile(hit_file)
            try:
                lock_handle.acquire(timeout=0)
                compute_hits_run(spi_config, run, series, num_panels, calib_run)
                lock_handle.release()
            except AlreadyLocked:
                continue
            except:
                lock_handle.release()
                raise
        else:
            compute_hits_run(spi_config, run, series, num_panels, calib_run)

    # Combine series data in one file
    if lock:
        lock_handle = LockFile(
            names.generate_hits_filename(spi_config.get_hits_directory(), run))
        try:
            lock_handle.acquire(timeout=0)
            combine_run_data.combine_run_data(spi_config, run)
            lock_handle.release()
        except AlreadyLocked:
            return
        except:
            lock_handle.release()
            raise
    else:
        combine_run_data.combine_run_data(spi_config, run)


def main():
    parser = argparse.ArgumentParser(description="Perform hit-search and full save for SPI data")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    parser.add_argument("-r", "--run", type=int, nargs='+', help="Run to process")
    parser.add_argument("--no-lock", dest='nolock', action='store_true', help="Do not use lock files")
    args = parser.parse_args()

    config_file = args.config_file
    data_runs = args.run
    lock = not args.nolock

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)
    log.setup_logging(spi_config.get_log_file())

    if data_runs is None:
        sample_runs = spi_config.get_sample_runs()
        data_runs = sum(sample_runs.values(), [])

    if len(data_runs) == 0:
        log.debug('No runs to process, fill SAMPLES section in config OR use "-r RUN" argument')
        return

    for run in data_runs:
        create_hits_run(spi_config, run, lock)

    log.debug('Hit-filtering completed')


if __name__ == '__main__':
    main()
