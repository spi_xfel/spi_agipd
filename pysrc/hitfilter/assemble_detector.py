#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Assemble panel data found hits
Author: Sergey Bobkov
"""

import sys
import os
import argparse
from lockfile import LockFile, AlreadyLocked
import numpy as np

import hitdata

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs.config import SpiConfig, CONFIG_FILE
from libs import agipd, names, log


# Copied from CrystFEL geometry file conversion scripts by Andrew Morgan
def pixel_maps_from_geometry_file(fnam, return_dict=False):
    """
    Return pixel and radius maps from the geometry file

    Input: geometry filename

    Output: x: slab-like pixel map with x coordinate of each slab pixel in the reference system of the detector
            y: slab-like pixel map with y coordinate of each slab pixel in the reference system of the detector
            z: slab-like pixel map with distance of each pixel from the center of the reference system.

    Note:
            ss || y
            fs || x

            vectors should be given by [x, y]
    """
    f = open(fnam, 'r')
    f_lines = []
    for line in f:
        f_lines.append(line)

    keyword_list = ['min_fs', 'min_ss', 'max_fs', 'max_ss', 'fs', 'ss', 'corner_x', 'corner_y']

    detector_dict = {}

    panel_lines = [x for x in f_lines if '/' in x]

    for pline in panel_lines:
        items = pline.split('=')[0].split('/')
        if len(items) == 2 :
            panel = items[0].strip()
            prop = items[1].strip()
            if prop in keyword_list and ('bad' not in panel and 'rigid' not in panel):
                if panel not in detector_dict.keys():
                    detector_dict[panel] = {}
                detector_dict[panel][prop] = pline.split('=')[1].split(';')[0].strip()

    parsed_detector_dict = {}

    for p in detector_dict.keys():

        parsed_detector_dict[p] = {}

        parsed_detector_dict[p]['min_fs'] = int(detector_dict[p]['min_fs'])
        parsed_detector_dict[p]['max_fs'] = int(detector_dict[p]['max_fs'])
        parsed_detector_dict[p]['min_ss'] = int(detector_dict[p]['min_ss'])
        parsed_detector_dict[p]['max_ss'] = int(detector_dict[p]['max_ss'])
        parsed_detector_dict[p]['fs'] = []
        parsed_detector_dict[p]['fs'].append(float(detector_dict[p]['fs'].split('x')[0]))
        parsed_detector_dict[p]['fs'].append(float(detector_dict[p]['fs'].split('x')[1].split('y')[0]))
        parsed_detector_dict[p]['ss'] = []
        parsed_detector_dict[p]['ss'].append(float(detector_dict[p]['ss'].split('x')[0]))
        parsed_detector_dict[p]['ss'].append(float(detector_dict[p]['ss'].split('x')[1].split('y')[0]))
        parsed_detector_dict[p]['corner_x'] = float(detector_dict[p]['corner_x'])
        parsed_detector_dict[p]['corner_y'] = float(detector_dict[p]['corner_y'])

    max_slab_fs = np.array([parsed_detector_dict[k]['max_fs'] for k in parsed_detector_dict.keys()]).max()
    max_slab_ss = np.array([parsed_detector_dict[k]['max_ss'] for k in parsed_detector_dict.keys()]).max()

    x = np.zeros((max_slab_ss+1, max_slab_fs+1), dtype=np.float32)
    y = np.zeros((max_slab_ss+1, max_slab_fs+1), dtype=np.float32)

    for p in parsed_detector_dict.keys():
        # get the pixel coords for this asic
        i, j = np.meshgrid(np.arange(parsed_detector_dict[p]['max_ss'] - parsed_detector_dict[p]['min_ss'] + 1),
                           np.arange(parsed_detector_dict[p]['max_fs'] - parsed_detector_dict[p]['min_fs'] + 1), indexing='ij')

        #
        # make the y-x ( ss, fs ) vectors, using complex notation
        dx = parsed_detector_dict[p]['fs'][1] + 1J * parsed_detector_dict[p]['fs'][0]
        dy = parsed_detector_dict[p]['ss'][1] + 1J * parsed_detector_dict[p]['ss'][0]
        r_0 = parsed_detector_dict[p]['corner_y'] + 1J * parsed_detector_dict[p]['corner_x']

        r = i * dy + j * dx + r_0

        y[parsed_detector_dict[p]['min_ss']: parsed_detector_dict[p]['max_ss'] + 1, parsed_detector_dict[p]['min_fs']: parsed_detector_dict[p]['max_fs'] + 1] = r.real
        x[parsed_detector_dict[p]['min_ss']: parsed_detector_dict[p]['max_ss'] + 1, parsed_detector_dict[p]['min_fs']: parsed_detector_dict[p]['max_fs'] + 1] = r.imag

    if return_dict:
        return x, y, parsed_detector_dict
    else:
        return x, y


def assemble_data_series(spi_config, run, series):
    hit_file = names.generate_hits_series_filename(spi_config.get_hits_directory(), run, series)

    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)
    _, n_panels = det_reader.get_num_series_panels(run)

    geom_fname = spi_config.get_geom_filename()
    geom_x, geom_y = pixel_maps_from_geometry_file(geom_fname)
    size_y = 2 * int(max(abs(geom_y.max()), abs(geom_y.min()))) + 2
    size_x = 2 * int(max(abs(geom_x.max()), abs(geom_x.min()))) + 2
    # convert y x values to idx values
    y_idx = np.array(geom_y, dtype=np.int) + size_y//2 - 1
    x_idx = np.array(geom_x, dtype=np.int) + size_x//2 - 1

    # Each instrument has data for one AGIPD cell
    instrument_ids = hitdata.get_instrument_ids(hit_file) 

    if len(instrument_ids) == 0:
        log.error('No data for run {} series {}'.format(run, series))

    # Assemble masks 
    assembled_masks = []
    for i, instrument_id in enumerate(instrument_ids):
        mask = hitdata.read_detector_mask(hit_file, instrument_id, 0)
        if mask is None:
            assembled_masks.append(None)
            continue

        panel_mask = [mask]
        panel_mask.extend(hitdata.read_detector_mask(hit_file, instrument_id, p_num)
                          for p_num in range(1, n_panels))
        panel_mask = np.stack(panel_mask)

        result_mask = np.ones((size_y, size_x), dtype=mask.dtype)
        result_mask[y_idx.flatten(), x_idx.flatten()] = panel_mask.flatten()
        
        assembled_masks.append(result_mask)
    
    if all(val is None for val in assembled_masks): # Empty data
        hitdata.save_image_data(hit_file, 1, None, None)
        return

    # Combine cells with equal masks
    mask_dict = {}
    instrument_keys = np.zeros_like(instrument_ids)
    for i, mask in enumerate(assembled_masks):
        if not mask_dict:
            key = 1
        else:
            key = max(mask_dict.keys()) + 1
            for k, val in mask_dict.items():
                if (val == mask).all():
                    key = k
                    break

        if key not in mask_dict.keys():
            mask_dict[key] = mask

        instrument_keys[i] = key

    image_ids = np.unique(instrument_keys[instrument_keys > 0])

    for image_id in image_ids:
        result_trains = []
        result_cells = []
        for i, instrument_id in enumerate(instrument_ids):
            if instrument_keys[i] != image_id:
                continue

            hit_train, hit_cell = hitdata.read_hits_ids_instrument(hit_file, instrument_id)
            result_trains.append(hit_train)
            result_cells.append(hit_cell)

        result_trains = np.concatenate(result_trains)
        result_cells = np.concatenate(result_cells)
        hitdata.save_hits_ids_image(hit_file, image_id, result_trains, result_cells)

        n_frames = len(result_trains)
        result_image = np.zeros((n_frames, size_y, size_x), dtype='u4')

        series_shift = 0

        for i, instrument_id in enumerate(instrument_ids):
            if instrument_keys[i] != image_id:
                continue

            panel_data = []
            for p_num in range(n_panels):
                data = hitdata.read_detector_data(hit_file, instrument_id, p_num)
                panel_data.append(data)

            n_series_frames = len(data)
            panel_data = np.stack(panel_data)

            for j in range(n_series_frames):
                result_image[j + series_shift, y_idx.flatten(), x_idx.flatten()] = \
                    panel_data[:, j].flatten()

            series_shift += n_series_frames

        hitdata.save_image_data(hit_file, image_id, result_image, mask_dict[image_id])
        hitdata.save_image_center(hit_file, image_id, np.array([size_x/2, size_y/2, 0]))

    return 0


def assemble_data_run(spi_config, run):
    geom_fname = spi_config.get_geom_filename()
    if not os.path.isfile(geom_fname):
        log.error('Geometry file does not exist: ' + geom_fname)
        return -1

    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)
    n_series, _ = det_reader.get_num_series_panels(run)

    for series in range(n_series):
        hit_file = names.generate_hits_series_filename(spi_config.get_hits_directory(), run, series)

        if not os.path.isfile(hit_file):
            log.error('No hits data for run {} series {}'.format(run, series))
            return -1

        lock = LockFile(hit_file)
        try:
            lock.acquire(timeout=0)
            log.debug('Assembling data for run {} series {}'.format(run, series))
            assemble_data_series(spi_config, run, series)
            log.info('Data assembled for run {} series {}'.format(run, series))
            lock.release()
        except AlreadyLocked:
            continue
        except:
            lock.release()
            raise


def main():
    parser = argparse.ArgumentParser(description="Assemble hits data in one image")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    parser.add_argument("-r", "--run", type=int, nargs='+', help="Run to process")
    args = parser.parse_args()

    config_file = args.config_file
    data_runs = args.run

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)
    log.setup_logging(spi_config.get_log_file())

    if data_runs is None:
        sample_runs = spi_config.get_sample_runs()
        data_runs = sum(sample_runs.values(), [])

    for run in data_runs:
        assemble_data_run(spi_config, run)


if __name__ == '__main__':
    main()
