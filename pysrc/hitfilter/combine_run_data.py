#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Extract and save data for found hits
Author: Sergey Bobkov
"""

import sys
import os
import argparse
import hitdata

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs.config import SpiConfig, CONFIG_FILE
from libs import names, agipd, cxidata, log


def combine_run_data(spi_config, run):
    hit_dir = spi_config.get_hits_directory()
    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)
    n_series, _ = det_reader.get_num_series_panels(run)

    run_hitfile = names.generate_hits_filename(hit_dir, run)

    if hitdata.get_hit_status(run_hitfile) == hitdata.HitStatus.READY:
        return 0

    hit_series_files = []
    for series in range(n_series):
        hit_file = names.generate_hits_series_filename(hit_dir, run, series)

        if not os.path.isfile(hit_file):
            log.error('No hits data for run {} series {}'.format(run, series))
            return -1

        if hitdata.get_hit_status(hit_file) != hitdata.HitStatus.READY:
            log.error('Data for run {} series {} is not ready'.format(run, series))
            return -1

        hit_series_files.append(hit_file)

    cxidata.combine_files(hit_series_files, run_hitfile, cxi_mode=True)
    log.info('Data combined for run {}'.format(run))

    return 0


def main():
    parser = argparse.ArgumentParser(description="Combine series data into one run file")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    parser.add_argument("-r", "--run", type=int, nargs='+', help="Run to process")
    args = parser.parse_args()

    config_file = args.config_file
    data_runs = args.run

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)
    log.setup_logging(spi_config.get_log_file())

    if data_runs is None:
        sample_runs = spi_config.get_sample_runs()
        data_runs = sum(sample_runs.values(), [])

    for run in data_runs:
        combine_run_data(spi_config, run)


if __name__ == '__main__':
    main()
