#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Extract and save data for found hits
Author: Sergey Bobkov
"""

import os
import sys
import argparse
from lockfile import LockFile, AlreadyLocked
import numpy as np

import hitdata

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs.config import SpiConfig, CONFIG_FILE
from libs import names, agipd, log
from calibration import calibration


def convert_photons(adu, noise_threshold, photon_adu_ratio):
    photons = np.clip(adu, noise_threshold, np.amax(adu)).astype(np.float)
    photons -= noise_threshold
    return np.ceil(np.divide(photons, photon_adu_ratio)).astype(adu.dtype)


def save_hits_panel(spi_config, run, series, panel, calib_run):
    raw_dir = spi_config.get_raw_directory()
    calib_dir = spi_config.get_calib_directory()
    noise_thresh = spi_config.get_noise_thresh()
    photon_adu_ratio = spi_config.get_photon_adu_ratio()

    det_reader = agipd.AGIPDReader(raw_dir, raw_data=True)
    image_shape = det_reader.get_image_shape(run)

    hit_file = names.generate_hits_series_filename(spi_config.get_hits_directory(), run, series)
    instrument_ids = hitdata.get_instrument_ids(hit_file)

    if len(instrument_ids) == 0 and panel == 0:
        log.error('No data for run {} series {}'.format(run, series))

    for instrument_id in instrument_ids:
        hit_train, hit_cell = hitdata.read_hits_ids_instrument(hit_file, instrument_id)

        if hit_train is None:
            hitdata.save_detector_data(hit_file, instrument_id, panel, None, None)
            continue

        assert (hit_cell == hit_cell[0]).all(), 'Instrument have data for multiple cells'
        cid = hit_cell[0]

        hit_series, hit_indexes = det_reader.get_index(run, panel, hit_train, hit_cell)

        data_shape = (len(hit_train),) + image_shape

        data = np.zeros(data_shape, dtype='u4')
        mask = calibration.get_image_mask(calib_dir, calib_run, panel, hit_cell[0])

        for i, tid in enumerate(hit_train):
            idx = hit_indexes[i]
            sid = hit_series[i]

            if idx < 0:
                log.warning('Cannot find hit-data for run:{} panel:{} train:{}, cell:{}'\
                            .format(run, panel, tid, cid))
                data[i] = 0
                continue

            if sid != series:
                log.warning('Different series {} != {} for hit'.format(sid, series) +\
                            ' in run:{} panel:{} train:{}, cell:{}'.format(run, panel, tid, cid))

            image_data = det_reader.read_image(run, sid, panel, idx)
            image_adu = calibration.calibrate_image(calib_dir, image_data, panel, cid, calib_run)
            image_photons = convert_photons(image_adu, noise_thresh, photon_adu_ratio)
            data[i] = image_photons.astype(np.uint32)

        hitdata.save_detector_data(hit_file, instrument_id, panel, data, mask)


def save_hits_run(spi_config, run):
    calib_run = calibration.get_best_calib_run(spi_config, run)
    if calib_run < 0:
        log.error('No calibration runs in config')
        return -1

    if not calibration.validate_calib_run(spi_config, calib_run):
        log.error('No data for calibration run {}'.format(calib_run))
        return -1

    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)
    n_series, n_panels = det_reader.get_num_series_panels(run)

    for series in range(n_series):
        hit_file = names.generate_hits_series_filename(spi_config.get_hits_directory(), run, series)

        if not os.path.isfile(hit_file):
            log.error('No hits data for run {} series {}'.format(run, series))
            return -1

        lock = LockFile(hit_file)
        try:
            lock.acquire(timeout=0)
            log.debug('Started saving hits for run {} series {}'.format(run, series))
            # Reversed for saving 0 panel last, need it to check hit status
            for panel in reversed(range(n_panels)):
                save_hits_panel(spi_config, run, series, panel, calib_run)
                log.debug('Data saved for panel {}'.format(panel))

            log.info('Hits saved for run {} series {}'.format(run, series))
            lock.release()
        except AlreadyLocked:
            continue
        except:
            lock.release()
            raise


def main():
    parser = argparse.ArgumentParser(description="Save data for hits in SPI data")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    parser.add_argument("-r", "--run", type=int, nargs='+', help="Run to process")
    args = parser.parse_args()

    config_file = args.config_file
    data_runs = args.run

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)
    log.setup_logging(spi_config.get_log_file())

    if data_runs is None:
        sample_runs = spi_config.get_sample_runs()
        data_runs = sum(sample_runs.values(), [])

    for run in data_runs:
        save_hits_run(spi_config, run)


if __name__ == '__main__':
    main()
    