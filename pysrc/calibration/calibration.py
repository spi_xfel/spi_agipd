#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Lib for calibration related functions
Author: Sergey Bobkov
"""

import os
import sys
import h5py
import numpy as np
from numba import njit
from lockfile import LockFile, AlreadyLocked

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs import names, agipd, log
from libs.config import SpiConfig


def get_best_calib_run(spi_config, data_run):
    calib_runs = spi_config.get_calib_runs()
    calib_runs = set_proper_calib_keys(calib_runs).keys()

    if len(calib_runs) == 0:
        return -1

    good_calib_runs = [cr for cr in calib_runs if cr < data_run]

    return max(good_calib_runs)


def validate_calib_run(spi_config, calib_run, lock_is_valid=False):
    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)
    calib_dir = spi_config.get_calib_directory()

    _, n_panels = det_reader.get_num_series_panels(calib_run)
    for i in range(n_panels):
        calib_file = names.generate_calib_filename(calib_dir, calib_run, i)
        if not validate_calib_file(calib_file):
            if not lock_is_valid or not LockFile(calib_file).is_locked():
                return False
    return True


def validate_calib_file(calib_file):
    if not os.path.isfile(calib_file):
        return False
    with h5py.File(calib_file, 'r') as h5calib:
        if SpiConfig.h5calib_analog_offset_name not in h5calib.keys() or \
                SpiConfig.h5calib_gain_thresh_name not in h5calib.keys() or \
                SpiConfig.h5calib_bad_pix_name not in h5calib.keys():
            return False
    return True


def set_proper_calib_keys(calib_runs):
    new_calib_runs = {}
    for _, val in calib_runs.items():
        new_calib_runs[val[0]] = val
    return new_calib_runs


def mask_pixels_above_10std(data):
    mean_value = np.mean(data)
    std_value = np.std(data)
    hot_pixels = data > (mean_value + 10*std_value)
    return hot_pixels


def get_run_panel_stats(spi_config, run, panel, chunk_size=50):
    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)
    n_series, _ = det_reader.get_num_series_panels(run)

    panel_mean = None
    panel_max = None
    panel_min = None
    processed_trains = 0

    for series in range(n_series):
        train_ids, _, _ = det_reader.read_ids(run, series, panel)
        for i in range(0, len(train_ids), chunk_size):
            chunk_train_ids = train_ids[i:i+chunk_size]
            image_data = det_reader.read_series(run, series, panel, train_ids=chunk_train_ids)
            chunk_mean = np.mean(image_data, axis=0)
            chunk_max = np.amax(image_data, axis=0)
            chunk_min = np.amin(image_data, axis=0)
            del image_data

            if processed_trains == 0:
                panel_mean = chunk_mean
                panel_max = chunk_max
                panel_min = chunk_min
            else:
                panel_mean = np.average(np.stack([chunk_mean, panel_mean]),
                                        weights=[len(chunk_train_ids), processed_trains],
                                        axis=0)
                panel_max = np.amax(np.stack([chunk_max, panel_max]), axis=0)
                panel_min = np.amin(np.stack([chunk_min, panel_min]), axis=0)
            
            processed_trains += len(chunk_train_ids)

    log.debug('Finished reading data for panel {} run {}'.format(panel, run))

    return panel_mean, panel_max, panel_min


def calibrate_panel(spi_config, panel, high_gain_run, medium_gain_run, low_gain_run):
    log.debug('Started calibration for run {}, panel {}'.format(high_gain_run, panel))
    calib_file = names.generate_calib_filename(spi_config.get_calib_directory(),
                                               high_gain_run, panel)

    # Compute mean,min,max values for each pixel of each cell for ADU and gain
    p_mean_hg, p_max_hg, p_min_hg = get_run_panel_stats(spi_config, high_gain_run, panel)
    p_mean_mg, p_max_mg, p_min_mg = get_run_panel_stats(spi_config, medium_gain_run, panel)
    p_mean_lg, p_max_lg, p_min_lg = get_run_panel_stats(spi_config, low_gain_run, panel)

    train_shape = p_mean_hg[:, 0].shape
    data_type = p_mean_hg.dtype

    with h5py.File(calib_file, 'w') as h5calib:
        offset = h5calib.create_dataset(SpiConfig.h5calib_analog_offset_name,
                                        shape=(3,)+train_shape, dtype=data_type)
        # Write ADU offset for 3 gain levels
        offset[0] = p_mean_hg[:, 0]
        offset[1] = p_mean_mg[:, 0]
        offset[2] = p_mean_lg[:, 0]

        thresh = h5calib.create_dataset(SpiConfig.h5calib_gain_thresh_name,
                                        shape=(2,)+train_shape, dtype=data_type)
        # Write 2 threshold values between maximin of high gain and minimum of meduim gain
        thresh[0] = p_max_hg[:, 1] + (p_min_mg[:, 1] - p_max_hg[:, 1])/2
        # and between maximin of meduim gain and minimum of low gain
        thresh[1] = p_max_mg[:, 1] + (p_min_lg[:, 1] - p_max_mg[:, 1])/2

        badpixel = h5calib.create_dataset(SpiConfig.h5calib_bad_pix_name,
                                          shape=train_shape, dtype='b', fillvalue=False)
        # Save pixels with overlapping ranges of gain values for different gains (bad gain pixels)
        badpixel[p_min_mg[:, 1] < p_max_hg[:, 1]] = True
        badpixel[p_min_lg[:, 1] < p_max_mg[:, 1]] = True

        # Save pixels with very high signal deviation without beam (bad adu pixels)
        badpixel[mask_pixels_above_10std(p_max_hg[:, 0] - p_min_hg[:, 0])] = True
        badpixel[mask_pixels_above_10std(p_max_mg[:, 0] - p_min_mg[:, 0])] = True
        badpixel[mask_pixels_above_10std(p_max_lg[:, 0] - p_min_lg[:, 0])] = True

        # Save pixels with fixed values
        badpixel[p_max_hg[:, 0] == p_min_hg[:, 0]] = True
        badpixel[p_max_mg[:, 0] == p_min_mg[:, 0]] = True
        badpixel[p_max_lg[:, 0] == p_min_lg[:, 0]] = True

    log.info('Finished calibration for run {}, panel {}'.format(high_gain_run, panel))


def create_calib_data(spi_config, run, panels=None, lock=True):
    validate_raw_data_for_calib(spi_config, run)

    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)

    config_calib_runs = set_proper_calib_keys(spi_config.get_calib_runs())

    high_gain_run, medium_gain_run, low_gain_run = config_calib_runs[run]
    assert run == high_gain_run, 'Bab calibration run key'

    _, n_panels = det_reader.get_num_series_panels(high_gain_run)

    if panels is None:
        panels = range(n_panels)
    else:
        for p in panels:
            assert p < n_panels, 'Data for panel {} is not available'.format(p)

    for panel in panels:
        calib_file = names.generate_calib_filename(spi_config.get_calib_directory(), run, panel)
        os.makedirs(os.path.dirname(calib_file), exist_ok=True)

        if lock:
            lock_handle = LockFile(calib_file)
            try:
                lock_handle.acquire(timeout=0)
                if not validate_calib_file(calib_file):
                    calibrate_panel(spi_config, panel, high_gain_run, medium_gain_run, low_gain_run)
                lock_handle.release()
            except AlreadyLocked:
                continue
            except:
                lock_handle.release()
                raise
        else:
            calibrate_panel(spi_config, panel, high_gain_run, medium_gain_run, low_gain_run)


def validate_raw_data_for_calib(spi_config, calib_run):
    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)
    config_calib_runs = set_proper_calib_keys(spi_config.get_calib_runs())

    high_gain_run, medium_gain_run, low_gain_run = config_calib_runs[calib_run]

    num_series_hi, num_panels_hi = det_reader.get_num_series_panels(high_gain_run)
    num_series_med, num_panels_med = det_reader.get_num_series_panels(medium_gain_run)
    num_series_low, num_panels_low = det_reader.get_num_series_panels(low_gain_run)

    if num_panels_hi != num_panels_med or num_panels_med != num_panels_low:
        raise ValueError('Different number of panels in calibration runs')

    if num_series_hi == 0 or num_series_med == 0 or num_series_low == 0:
        raise ValueError('Missing data in calibration runs')


def get_gain_mode(gain_data, gain_thresh):
    high_gain = gain_data < gain_thresh[np.newaxis, 0]
    low_gain = gain_data > gain_thresh[np.newaxis, 1]
    medium_gain = (~high_gain) * (~low_gain)
    return low_gain*2 + medium_gain


@njit()
def _calibrate_series_numba(series_data, offset, gain_thresh, rel_gain, result_data):
    num_trains, num_cells, _, heigth, width = series_data.shape
    part_x = 64
    part_y = 64

    frame_adu = np.zeros((heigth, width), dtype=np.int32)
    frame_gain = np.zeros((heigth, width), dtype=np.int32)

    for train in range(num_trains):
        for cell in range(num_cells):
            for y in range(heigth):
                for x in range(width):
                    adu = int(series_data[train, cell, 0, y, x])
                    gain = series_data[train, cell, 1, y, x]
                    if gain < gain_thresh[0, cell, y, x]:
                        gain_mode = 0
                    elif gain < gain_thresh[1, cell, y, x]:
                        gain_mode = 1
                    else:
                        gain_mode = 2

                    adu -= offset[gain_mode, cell, y, x]
                    frame_adu[y, x] = adu
                    frame_gain[y, x] = gain_mode
                    # if adu > 0:
                    #     result_data[train, cell, y, x] = adu * rel_gain[gain_mode]

            # Common mode correct for individual parts
            for y in range(0, heigth, part_y):
                for x in range(0, width, part_x):
                    median_adu = np.median(frame_adu[y:y+part_y, x:x+part_x])
                    for yy in range(part_y):
                        for xx in range(part_x):
                            adu = frame_adu[y+yy, x+xx]
                            gain_mode = frame_gain[y+yy, x+xx]
                            if gain_mode == 0:
                                adu -= median_adu

                            if adu > 0:
                                result_data[train, cell, y+yy, x+xx] = adu * rel_gain[gain_mode]


def calibrate_series(calib_dir, series_data, panel, calib_run, cell_range=None, mask_bad_pixels=True):
    calib_file = names.generate_calib_filename(calib_dir, calib_run, panel)
    relative_gain = np.array([1, 38, 183])

    with h5py.File(calib_file, 'r') as h5calib_file:
        h5_offset = h5calib_file[SpiConfig.h5calib_analog_offset_name]
        h5_thresh = h5calib_file[SpiConfig.h5calib_gain_thresh_name]
        h5_badpixel = h5calib_file[SpiConfig.h5calib_bad_pix_name]

        total_cells = h5_offset.shape[1]

        cell_mask = np.zeros(total_cells, dtype=bool)
        if cell_range is None:
            cell_mask[:] = True
        else:
            cell_mask[cell_range] = True

        offset = h5_offset[:, cell_mask]
        thresh = h5_thresh[:, cell_mask]
        badpixel = h5_badpixel[cell_mask, ...]

        series_adu = np.zeros(np.array(series_data.shape)[[0, 1, 3, 4]], dtype='u4')
        _calibrate_series_numba(series_data, offset, thresh, relative_gain, series_adu)

        if mask_bad_pixels:
            for i in range(len(series_adu)):
                np.putmask(series_adu[i], badpixel, 0)

    return series_adu


@njit()
def _calibrate_image_numba(image_data, offset, gain_thresh, rel_gain, result_data):
    _, heigth, width = image_data.shape
    part_x = 64
    part_y = 64

    frame_adu = np.zeros((heigth, width), dtype=np.int32)
    frame_gain = np.zeros((heigth, width), dtype=np.int32)

    for y in range(heigth):
        for x in range(width):
            adu = int(image_data[0, y, x])
            gain = image_data[1, y, x]
            if gain < gain_thresh[0, y, x]:
                gain_mode = 0
            elif gain < gain_thresh[1, y, x]:
                gain_mode = 1
            else:
                gain_mode = 2

            adu -= offset[gain_mode, y, x]
            frame_adu[y, x] = adu
            frame_gain[y, x] = gain_mode

    # Common mode correct for individual parts
    for y in range(0, heigth, part_y):
        for x in range(0, width, part_x):
            median_adu = np.median(frame_adu[y:y+part_y, x:x+part_x])
            for yy in range(part_y):
                for xx in range(part_x):
                    adu = frame_adu[y+yy, x+xx]
                    gain_mode = frame_gain[y+yy, x+xx]
                    if gain_mode == 0:
                        adu -= median_adu

                    if adu > 0:
                        result_data[y+yy, x+xx] = adu * rel_gain[gain_mode]


def calibrate_image(calib_dir, image_data, panel, cell_id, calib_run, mask_bad_pixels=True):
    calib_file = names.generate_calib_filename(calib_dir, calib_run, panel)
    relative_gain = np.array([1, 38, 183])

    with h5py.File(calib_file, 'r') as h5calib_file:
        h5_offset = h5calib_file[SpiConfig.h5calib_analog_offset_name]
        h5_thresh = h5calib_file[SpiConfig.h5calib_gain_thresh_name]
        h5_badpixel = h5calib_file[SpiConfig.h5calib_bad_pix_name]

        offset = h5_offset[:, cell_id]
        thresh = h5_thresh[:, cell_id]
        badpixel = h5_badpixel[cell_id, ...]

        image_adu = np.zeros(image_data.shape[1:], dtype='u4')

        _calibrate_image_numba(image_data, offset, thresh, relative_gain, image_adu)

        if mask_bad_pixels:
            np.putmask(image_adu, badpixel, 0)

    return image_adu


def get_image_mask(calib_dir, calib_run, panel, cell):
    calib_file = names.generate_calib_filename(calib_dir, calib_run, panel)

    with h5py.File(calib_file, 'r') as h5calib_file:
        badpixel = h5calib_file[SpiConfig.h5calib_bad_pix_name][cell]

    return badpixel
