#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Check possibility to create calibration data
Author: Sergey Bobkov
"""

import os
import sys
import argparse
import numpy as np

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from calibration import calibration
from libs.config import SpiConfig, CONFIG_FILE
from libs import names, log


def main():
    parser = argparse.ArgumentParser(description="Check possibility to create calibration data")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    args = parser.parse_args()

    config_file = args.config_file

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)

    config_calib_runs = calibration.set_proper_calib_keys(spi_config.get_calib_runs())

    if not config_calib_runs:
        log.error('No calibration runs in config')
        sys.exit(1)

    calib_runs = sorted(config_calib_runs.keys())

    run_status = np.zeros(len(calib_runs), dtype=int)

    for i, run in enumerate(calib_runs):
        if calibration.validate_raw_data_for_calib(spi_config, run):
            if calibration.validate_calib_run(spi_config, run, lock_is_valid=True):
                run_status[i] = 1
            else:
                run_status[i] = 0

        else:
            run_status[i] = -1

    if (run_status == 1).all():
        sys.exit(10) # All calibration is done
    elif (run_status == 0).any():
        sys.exit(0) # Some data is ready for calibration
    else:
        sys.exit(1) # No more data for calibration, yet


if __name__ == '__main__':
    main()
