#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Print status of hits data
Author: Sergey Bobkov
"""

import os
import sys
import argparse

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from libs.config import SpiConfig, CONFIG_FILE
from libs import names, agipd, log
from hitfilter import hitdata


def main():
    parser = argparse.ArgumentParser(description="Print status of SPI data")
    parser.add_argument("-c", "--config", dest='config_file', default=CONFIG_FILE,
                        help="Config file")
    args = parser.parse_args()

    config_file = args.config_file

    if not os.path.isfile(config_file):
        parser.error('Config file {} does not exist'.format(config_file))

    spi_config = SpiConfig(config_file, set_work_dir=True)
    log.setup_logging(spi_config.get_log_file())

    det_reader = agipd.AGIPDReader(spi_config.get_raw_directory(), raw_data=True)

    hit_dir = spi_config.get_hits_directory()
    if not os.path.isdir(hit_dir):
        log.debug("Hits folder doesn't exist")

    panel = spi_config.get_brightest_panel()

    runs = det_reader.get_runs()

    print('Run\tSeries\tFrames\tHits\tHit status')
    for run in runs:
        n_series, _ = det_reader.get_num_series_panels(run)

        num_frames = sum(det_reader.get_num_frames(run, series, panel)\
                         for series in range(n_series))

        hit_file = names.generate_hits_filename(hit_dir, run)
        hit_status = hitdata.get_hit_status(hit_file)
        if hit_status == hitdata.HitStatus.EMPTY:
            status_line = 'None'
        elif hit_status == hitdata.HitStatus.LOCKED:
            status_line = 'In progress...'
        elif hit_status == hitdata.HitStatus.FOUND_IDS:
            status_line = 'Found events'
        elif hit_status == hitdata.HitStatus.SAVED:
            status_line = 'Unassembled data'
        else:
            status_line = 'Ready'

        if hit_status == hitdata.HitStatus.READY:
            num_hits = hitdata.get_num_frames(hit_file)
        else:
            num_hits = 0

        print('{}\t{}\t{}\t{}\t{}'.format(run, n_series, num_frames, num_hits, status_line))


if __name__ == '__main__':
    main()
    