#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

CONTAINER_CONFIG = 'containers.config'


def load_container_config():
    os.chdir(os.path.dirname(__file__))
    config = {}
    if os.path.isfile(CONTAINER_CONFIG):
        for line in open(CONTAINER_CONFIG, 'r'):
            key, value = line.split('=')
            config[key] = value

    return config


def get_raw_dir(config_file):
    with open(config_file, 'r') as infile:
        for line in infile:
            if line.split()[0] == 'raw_directory':
                return line.split()[-1]
