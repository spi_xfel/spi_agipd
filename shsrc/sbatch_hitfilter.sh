#! /bin/sh

#SBATCH -o calib-%j.out
#SBATCH -e calib-%j.err
#SBATCH -p hpc4-3d
#SBATCH -n 1
#SBATCH --cpus-per-task 2


`which python` pysrc/hitfilter/create_hits_data.py -c config.ini


