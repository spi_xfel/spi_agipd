# Requirements

Processing scripts require Python 3 with libraries:
* NumPy,
* SciPy,
* Numba,
* h5py,
* Matplotlib.
* lockfile

It is recommended to use [Anaconda](https://www.anaconda.com/products/individual#Downloads) distribution.

# Calibration and hit-filtering for AGIPD detector on European XFEL.

To process the experiment data, first you need to create a new processing folder:
```
python init_new.py [-h] -r RAW [-t TAG] [-p PREFIX]
```
where **RAW** is path for raw data directory, **TAG** is the prefered folder name (a number will be added) and **PREFIX** is the location of a new processing folder.

Then you need to go into the new folder. You will find the following content in it:
- agipd.geom - Default geometry file for AGIPD. Other geometry files can be created by geoptimiser in [CrystFEL](https://www.desy.de/~twhite/crystfel/manual-geoptimiser.html) software.
- config.ini - Experiment configuration, details below.
- pysrc - processing scripts (symbolic link).

For calibration and hit-filtering multiple processes can be started, they will distribute calculations through file locking mechanism. Command line options can always be discovered with '-h' key.

## Configuration file

Configuration file default name in *config.ini*, the file includes following sections: 
1. **FILE_PATH** - data paths and filenames
2. **HIT_FILTER** - parameters of hit-filtering module:
    - **brightest_panel** - number of AGIPD panel with the highest scattering intensity;
    - **lit_pixels_threshold_std_multi** - threshold multiplier. Hit-filtering is based on number of lit-pixels per image and the threshold is:
        > *'threshold'* = *'mean'* + *'standart deviation'* * *'threshold multiplier'*
    - **cell_filter** - array containing numbers of AGIPD cells with good images. Can be defined as:
        ```
        - [1,2,5]
        - range(2,62,2)
        - [i for i in range(1,176) if i not in range(18,176,32)]
        ```
3. **PHOTON_CONVERTER** - photon conversion parameters
    - **photon_adu_ratio** - conversion ratio between detector ADUs and number of photons
    - **noise_threshold** - maximum ADU value that should still be considered as 0 photons.
    $$N_{photons}=RoundUp(\frac{ADU - noise\ threshold}{Ratio_{photon\ adu}})$$
    ADU values below noise_threshold converted into zero photons, values above noise_threshold but below noise_threshold + photon_adu_ratio converted into one photon values, and so forth.

4. **DETECTOR_ASSEMBLY**
    - **geometry_file** - path to *.geom file with AGIPD geometry that will be used for assembly of full detector images.

5. **CALIB_RUNS** - list of calibration runs with dark runs. Should be defined as: **A=X,Y,Z**, where:
    - **A** - run name, free format;
    - **X** - dark run number with forced high gain, it will also be used as a run of the resulting calibration data;
    - **Y** - dark run number with forced medium gain;
    - **Z** - dark run number with forced low gain.

    Example of a proper definition is:
    > r0021 = 21, 22, 23

6. **SAMPLES** - list of runs in which a sample data were collected. Should be defined as **SampleName = Run sequence**.
Run sequence is a comma-separated list of run numbers and ranges in form of **A-B**, both ends included. 
Example of a proper definition is:
    > virus = 21, 25-29, 31, 35-40, 44 

## Create calibration data

To create calibiration data use the following command:
```
python pysrc/calibration/create.py
```
This will start calculations of calibrations data for runs defined in configuration file. 

## Calibration and hit-filtering

Calibration of RAW data and hit-filtering are performed simultaneously. Selected hits are saved in CXI files. To start processing use:
```
python pysrc/hitfilter/create_hits_data.py
```
Hit-filtering is performed for runs in **samples** section of config file.
You can manually set run to process with *--run* argument.

Hit-filtering use calibration data from the latest run prior to processed data.

Data for each run is divided into series. Series are processed invididually. When data for all series are ready, it is combined into single file for this run.
If running multiple processes, different series can be processed in parallel.

Because data for different AGIPD cells often have different masks, result CXI file will have several image groups ("/entry_1/image_1", "/entry_1/image_2" and so on...). Each group contain 3D "data" array with dimensions (frame, y, x), 2D "mask" array and "image_center" array with 3 values (x, y, z).

Hit-filtering contain several steps, they can also be preformed individually:
1. Search for hits ids. 
    > `python pysrc/hitfilter/find_hits.py`

2. Extraction of calibrated data.
    > `python pysrc/hitfilter/save_hits_data.py`

3. Assembly of extracted data into full detector images.
    > `python pysrc/hitfilter/assemble_detector.py`
4. Removal of unassembled extracted data.
    > `python pysrc/hitfilter/delete_panels_data.py`
5. Collection of series data into single file for each run.
    > `python pysrc/hitfilter/combine_run_data.py`
